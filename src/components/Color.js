import React from "react";
import { SketchPicker } from "react-color";

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }
  handleChangeComplete = (color) => {
    this.props.handleChangeColor(color.hex);
  };

  render() {
    return (
      <SketchPicker
        color={this.props.color}
        onChangeComplete={this.handleChangeComplete}
      />
    );
  }
}
