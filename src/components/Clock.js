import React, { useEffect } from "react";

let interval = null
export default function Clock(props) {
  useEffect(() => {
    interval && clearInterval(interval)
    interval = setInterval(function () {
      function r(el, deg) {
        document
          .getElementById(el)
          .setAttribute("transform", "rotate(" + deg + " 50 50)");
      }
      var d = new Date(new Date () - parseFloat(props.offset)*60*60*1000);
      r("sec", 6 * d.getSeconds());
      r("min", 6 * d.getMinutes());
      r("hour", 30 * (d.getHours() % 12) + d.getMinutes() / 2);
    }, 1000);

  });

  return (
    <div id="clock-container">
      <svg id="clock" viewBox="0 0 100 100">
        <circle style={{fill: `${props.color}`}} id="face" cx="50" cy="50" r="45" />
        <g id="hands">
          <rect
            id="hour"
            x="47.5"
            y="16.5"
            width="3"
            height="33"
            rx="2.5"
            ry="2.55"
            transform="rotate(272 50 50)"
          ></rect>
          <rect
            id="min"
            x="48"
            y="16.5"
            width="2"
            height="33"
            rx="2"
            ry="2"
            transform="rotate(24 50 50)"
          ></rect>
          <line
            id="sec"
            x1="49"
            y1="50"
            x2="49"
            y2="17"
            transform="rotate(252 50 50)"
            style={{display: !props.showSecond ? `none` : ''}}
          ></line>
          <text x="45" y="14" fontFamily="Verdana" fontSize="7" fill="white">
            12
          </text>
          <text x="47" y="90" fontFamily="Verdana" fontSize="7" fill="white">
            6
          </text>
          <text x="10" y="52" fontFamily="Verdana" fontSize="7" fill="white">
            9
          </text>
          <text x="86" y="52" fontFamily="Verdana" fontSize="7" fill="white">
            3
          </text>
        </g>
      </svg>
    </div>
  );
}
