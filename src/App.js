import React, { useState } from 'react';
import './App.css';
import Clock from './components/Clock';
import ColorPicker from './components/Color';

function App() {
  const [color, setColor] = useState('#111');
  const [offset, setOffset] = useState(0);
  const [showSecond, setShowSecond] = useState(false);
  return (
    <div className="App">
      <Clock color={color} offset={offset} showSecond={showSecond} />
      <div className="right" style={{color: 'white', textAlign: 'left'}}>
        <div>Offset hours<input type="number" value={offset} onChange={(e) => setOffset(e.currentTarget.value)} placeholder="Offset" /></div>
        <div>Show second <input type="checkbox" value={setShowSecond} onChange={(e) => setShowSecond(e.target.checked)} /></div>
        <ColorPicker handleChangeColor={setColor}/>
      </div>
    </div>
  );
}

export default App;
